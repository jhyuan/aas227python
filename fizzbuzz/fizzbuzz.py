"""Do the fizzbuzz test"""

def fizzbuzz(num):

	result = ''
	# do something
	if (num % 3) == 0:
		result += 'fizz'
	if (num % 5) == 0:
		result += 'buzz'
	
	if not result:
		result = num
		
	return result
